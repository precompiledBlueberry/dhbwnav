use seed::log;
use three_d::*;
use wasm_bindgen::JsCast;
use web_sys::HtmlCanvasElement;

/// Create the canvas that shows the model
pub async fn init_canvas() {
    let canvas: Option<HtmlCanvasElement> = web_sys::window()
        .and_then(|w| w.document())
        .and_then(|d| d.get_element_by_id("modelviewer"))
        .and_then(|el| match el.dyn_into::<HtmlCanvasElement>() {
            Ok(s) => Some(s),
            Err(_err) => None,
        });

    let window = Window::new(WindowSettings {
        canvas,
        ..Default::default()
    })
    .unwrap();
    let context = window.gl();

    let mut camera = Camera::new_perspective(
        window.viewport(),
        vec3(4.0, 1.5, 4.0),
        vec3(0.0, 1.0, 0.0),
        vec3(0.0, 1.0, 0.0),
        degrees(45.0),
        0.1,
        1000.0,
    );
    let mut control = OrbitControl::new(*camera.target(), 1.0, 100.0);

    let mut loaded = three_d_asset::io::load_async(&["assets/BuildingA.glb"])
        .await
        .unwrap();

    let model = loaded.deserialize("BuildingA.glb");
    if let Err(error) = &model {
        log!(error.to_string());
    }
    let mut uni = Model::<PhysicalMaterial>::new(&context, &model.unwrap()).unwrap();
    uni.iter_mut().for_each(|m| {
        //m.set_transformation()
        m.material.render_states.cull = Cull::None;
    });

    let ambient = AmbientLight::new(&context, 0.4, Color::WHITE);

    window.render_loop(move |mut frame_input| {
        let mut redraw = frame_input.first_frame;
        redraw |= camera.set_viewport(frame_input.viewport);
        redraw |= control.handle_events(&mut camera, &mut frame_input.events);

        // TODO: read the path and highlight the sub-models
        // NOTE: although the glb file has submodels which are named,
        // NOTE: ... it's currently not possible to change the models' appereances :(

        if redraw {
            frame_input.screen().clear(ClearState::default()).render(
                &camera,
                uni.into_iter(),
                &[&ambient],
            );
        }

        FrameOutput {
            swap_buffers: redraw,
            ..Default::default()
        }
    });
}
