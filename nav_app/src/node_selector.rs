use std::vec;

use nav_lib::{Building, Node, NodeType, Query};
use seed::{prelude::*, *};

///! Select all rooms and submit it

pub struct NodeSelector {
    list_of_places: Vec<Node>,
    start: Node,
    end: Node,
}

#[derive(Debug)]
pub enum Msg {
    SelectStartChanged(String),
    SelectEndChanged(String),
    Submit,
    SubmitSuccesful(String),
    SubmitFailed(String),
    RecievedList(Vec<Node>),
}

// Function to init the selectors, as well as fetch the list of nodes from srv
impl NodeSelector {
    pub fn new(_url: Url, orders: &mut impl Orders<Msg>) -> NodeSelector {
        let request = Request::new("fetch_list");
        orders.perform_cmd(async {
            let response = request.fetch().await.unwrap();
            let list_of_places = response.bytes().await.unwrap();
            let list_of_places = serde_json::from_slice::<Vec<Node>>(&list_of_places).unwrap();
            Msg::RecievedList(list_of_places)
        });

        let list_of_places = vec![Node {
            floor: 0, // could be u8 but
            name: "Example".into(),
            building: Building::B,
            node_type: NodeType::LectureRoom,
        }];

        // TODO: Add cookies for start and end pos
        NodeSelector {
            // start and end have to be specified before moving list_of_places
            start: list_of_places[0].clone(),
            end: list_of_places[0].clone(),
            list_of_places,
        }
    }

    pub fn list_of_places(&self) -> impl Iterator<Item = &Node> {
        self.list_of_places.iter()
    }
}

pub fn update(msg: Msg, model: &mut NodeSelector, orders: &mut impl Orders<Msg>) {
    match msg {
        Msg::Submit => {
            orders.skip();

            let query = Query::new(&model.start, &model.end);
            let query_json = serde_json::to_string(&query).expect("Couldn't parse JSON");
            log!(query_json);

            let request = Request::new("user_submit")
                .method(Method::Post)
                .json(&query)
                .expect("Couldn't create request");

            orders.perform_cmd(async {
                let response = fetch(request).await.expect("HTTP request failed");

                if response.status().is_ok() {
                    Msg::SubmitSuccesful(response.text().await.unwrap())
                } else {
                    Msg::SubmitFailed(response.status().text)
                }
            });
        }
        Msg::SelectStartChanged(data) => {
            model.start = model.list_of_places[data.parse::<usize>().unwrap()].clone()
        }
        Msg::SelectEndChanged(data) => {
            model.end = model.list_of_places[data.parse::<usize>().unwrap()].clone()
        }
        Msg::SubmitFailed(error_msg) => log!(error_msg),
        Msg::SubmitSuccesful(response) => log!(response),
        Msg::RecievedList(array) => {
            model.start = array[0].clone();
            model.end = array[0].clone();
            model.list_of_places = array;
        }
    }
}

pub fn view(model: &NodeSelector) -> seed::prelude::Node<Msg> {
    div![
        C!("karen"),
        div!["Choose your starting point:", id!("starttext")],
        select![
            id!["start"],
            model
                .list_of_places()
                .enumerate()
                .map(|(i, node)| option!(node.name.clone(), attrs!(At::Value => i))),
            input_ev(Ev::Input, Msg::SelectStartChanged),
        ],
        div!["Endpoint:", id!("endtext")],
        select![
            id!["end"],
            model
                .list_of_places()
                .enumerate()
                .map(|(i, node)| option!(node.name.clone(), attrs!(At::Value => i))),
            input_ev(Ev::Input, Msg::SelectEndChanged),
        ],
        button![id!["submit"], "Submit", ev(Ev::Click, |_| Msg::Submit),]
    ]
}
