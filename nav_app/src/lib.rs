use node_selector::NodeSelector;
/* // Assert that this package doesn't get compiled for the wrong target
#[cfg(not(target_arch="wasm32"))]
compile_error!("This package requires wasm32");
 */
use seed::{prelude::*, *};

mod model_viewer;
mod node_selector;

// `init` describes what should happen when your app started.
fn init(url: Url, orders: &mut impl Orders<Msg>) -> Model {
    let node_selector =
        node_selector::NodeSelector::new(url, &mut orders.proxy(Msg::SelectorNames));

    let path = "Select your start and destination. Then click 'Submit'".into();

    Model {
        node_selector,
        path,
    }
}

// `Model` describes our app state.
struct Model {
    node_selector: NodeSelector,
    path: String,
}

#[derive(Debug)]
// `Msg` describes the different events you can modify state with.
enum Msg {
    SelectorNames(node_selector::Msg),
}

// `update` describes how to handle each `Msg`.
fn update(msg: Msg, model: &mut Model, orders: &mut impl Orders<Msg>) {
    web_sys::console::log_1(&format!("{:?}", msg).into());
    match msg {
        Msg::SelectorNames(node_selector::Msg::SubmitSuccesful(response)) => {
            let mut split = response.split(':');
            let (cost, path) = (split.next().unwrap(), split.next().unwrap());
            let path = path
                .replace("ELEA", "Aufzug A")
                .replace('T', "Treppenhaus ")
                .replace("Eingang", "Eingang ");
            let output = format!(
                "Follow these rooms: {}\n\nTakes (approx.) {:.2} minutes",
                path[1..].replace(',', "\n\t->"),
                (cost.parse::<f64>().unwrap() / 60.0).ceil()
            );
            model.path = output;
        }
        Msg::SelectorNames(msg) => node_selector::update(
            msg,
            &mut model.node_selector,
            &mut orders.proxy(Msg::SelectorNames),
        ),
    }
}

// `view` describes what to display.
fn view(model: &Model) -> Node<Msg> {
    div![
        C!("linda"),
        div![id!("Header"), "DHBWNav",],
        node_selector::view(&model.node_selector).map_msg(Msg::SelectorNames),
        pre![id!("model_object_viewer_canvas"), model.path.as_str()],
        div![
            id!("about"),
            "2022 ",
            a!(
                "Our Gitlab",
                attrs! {At::Href => "https://gitlab.com/precompiledBlueberry/dhbwnav/-/tree/main"}
            )
        ],
    ]
}

#[wasm_bindgen(start)]
pub async fn main() {
    // Mount the `app` to the element with the `id` "app".
    App::start("app", init, update, view);
    model_viewer::init_canvas().await;
}
