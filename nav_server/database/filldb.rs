use nav_lib::{Building, Node, NodeType};
use rusqlite::{Connection, OpenFlags, Result, Row};

/// Fills the database with data and test it
fn main() -> Result<()> {
    println!("hello world");
    let db = Connection::open_with_flags("navDB.db", OpenFlags::SQLITE_OPEN_READ_WRITE)?;

    db.execute("PRAGMA foreign_keys = ON", [])?;
    db.execute("DROP TABLE IF EXISTS conns;", [])?;
    db.execute("DROP TABLE IF EXISTS rooms;", [])?;

    db.execute(
        "
        create table if not exists rooms (
             r_id integer primary key autoincrement,
             name text not null,
             floor integer not null,
             building text not null,
             type text not null
        );",
        [],
    )
    .unwrap();

    db.execute(
        "create table if not exists conns (
        c_id integer primary key autoincrement,
        start integer REFERENCES rooms(r_id),
        end integer REFERENCES rooms(r_id),
        distance integer not null        
    );",
        [],
    )
    .unwrap();

    insert_rooms(&db)?;

    query_rooms(&db)?;

    insert_conns(&db)?;

    query_conns(&db)?;

    Ok(())
}

fn insert_rooms(db: &Connection) -> Result<()> {
    use NodeType::*;

    let a0_room = |name: &str, node_type| Node {
        building: Building::A,
        floor: 0,
        name: name.to_string(),
        node_type,
    };
    let a1_room = |name: &str, node_type| Node {
        building: Building::A,
        floor: 1,
        name: name.to_string(),
        node_type,
    };

    let mut node_list: Vec<Node> = vec![
        a0_room("001A", Maintenance),
        a0_room("002A", Maintenance),
        a0_room("003A", Maintenance),
        a0_room("004A", LectureRoom),
        a0_room("005A", LectureRoom),
        a0_room("006A", LectureRoom),
        a0_room("007A", Office),
        a0_room("008A", Office),
        a0_room("009A", Office),
        a0_room("009.1A", Office),
        a0_room("010A", LectureRoom),
        a0_room("011A", Office),
        a0_room("012A", Toilet),
        a0_room("013A", Toilet),
        a0_room("014A", Toilet),
        a0_room("015A", Office),
        a0_room("016A", Office),
        a0_room("017A", Office),
        a0_room("018A", Office),
        a0_room("019A", Office),
        a0_room("020A", Office),
        a0_room("020.1A", Office),
        a0_room("020.2A", Office),
        a0_room("022A", LectureRoom),
        a0_room("023A", Office),
        a0_room("024A", Office),
        a0_room("025A", Office),
        a0_room("026A", Office),
        a1_room("101A", Maintenance),
        a1_room("102A", Maintenance),
        a1_room("103A", Office),
        a1_room("104A", LectureRoom),
        a1_room("105A", LectureRoom),
        a1_room("106A", LectureRoom),
        a1_room("107A", LectureRoom),
        a1_room("108A", LectureRoom),
        a1_room("109A", Office),
        a1_room("110A", LectureRoom),
        a1_room("111A", Office),
        a1_room("112A", Office),
        a1_room("113A", Toilet),
        a1_room("114A", Maintenance),
        a1_room("115A", Toilet),
        a1_room("116A", LectureRoom),
        a1_room("117A", LectureRoom),
        a1_room("118A", LectureRoom),
        a1_room("120A", LectureRoom),
        a1_room("122A", LectureRoom),
        a1_room("123A", LectureRoom),
        Node {
            name: "EingangA1".to_string(),
            floor: 0,
            node_type: Ingress,
            building: Building::A,
        },
        Node {
            name: "EingangAB".to_string(),
            floor: 1,
            node_type: Ingress,
            building: Building::A,
        },
        Node {
            name: "TA1".to_string(),
            floor: 0,
            node_type: Stairwell,
            building: Building::A,
        },
        Node {
            name: "TA2".to_string(),
            floor: 0,
            node_type: Stairwell,
            building: Building::A,
        },
        Node {
            name: "ELEA".to_string(),
            floor: 0,
            node_type: Stairwell,
            building: Building::A,
        },
    ];
    node_list.push(a1_room("121A", LectureRoom));
    {
        let mut stmt = db.prepare_cached(
            "INSERT INTO rooms (name, floor, building, type) VALUES (?, ?, ?, ?)",
        )?;

        for node in node_list {
            stmt.execute((node.name, node.floor, node.building, node.node_type))?;
        }
    }

    Ok(())
}

fn insert_conns(db: &Connection) -> Result<()> {
    let conn_list = [
        (53, 5, 7),
        (5, 6, 15),
        (6, 7, 11),
        (7, 4, 9),
        (4, 8, 1),
        (8, 9, 6),
        (9, 10, 11),
        (10, 3, 13),
        (3, 11, 12),
        (11, 12, 2),
        (12, 2, 2),
        (2, 1, 6),
        (1, 50, 2),
        (50, 54, 2),
        (54, 52, 6),
        (52, 13, 1),
        (13, 14, 7),
        (14, 15, 7),
        (15, 16, 7),
        (16, 28, 2),
        (28, 17, 9),
        (17, 27, 1),
        (27, 18, 8),
        (18, 26, 9),
        (26, 19, 2),
        (19, 20, 8),
        (20, 25, 2),
        (25, 21, 10),
        (21, 24, 1),
        (24, 22, 9),
        (22, 24, 1),
        (24, 23, 7),
        // AE Fertig ab diesem Punkt
        (53, 33, 6),
        (33, 34, 13),
        (34, 35, 12),
        (35, 32, 7),
        (32, 36, 14),
        (36, 37, 9),
        (37, 31, 2),
        (31, 38, 4),
        (38, 30, 1),
        (30, 39, 12),
        (39, 29, 5),
        (29, 40, 1),
        (40, 54, 3),
        (52, 41, 1),
        (41, 42, 7),
        (42, 43, 7),
        (43, 49, 7),
        (49, 44, 6),
        (44, 48, 3),
        (48, 45, 12),
        (45, 55, 4),
        (55, 47, 15),
        (47, 46, 5),
        (46, 51, 13),
    ];

    let mut stmt =
        db.prepare_cached("INSERT INTO conns (start, end, distance) VALUES (?, ?, ?)")?;

    for connection in conn_list {
        stmt.execute((connection.0, connection.1, connection.2))?;
    }

    Ok(())
}

fn query_rooms(db: &Connection) -> Result<()> {
    let mut stmt = db.prepare("SELECT * FROM rooms")?;

    let rows = stmt.query_map([], |row: &Row| -> Result<Node> {
        let id: usize = row.get(0)?;
        println!("{} -> :)", id);

        let name = row.get(1)?;
        let floor = row.get(2)?;
        let building = row.get(3)?;
        let node_type = row.get(4)?;

        Ok(Node {
            name,
            floor,
            building,
            node_type,
        })
    })?;

    for r in rows {
        println!("{:?}", r);
    }

    Ok(())
}

fn query_conns(db: &Connection) -> Result<()> {
    let mut stmt = db.prepare("SELECT * FROM conns")?;

    let rows = stmt.query_map([], |row| Ok((row.get(0)?, row.get(1)?, row.get(2)?)))?;

    for row in rows {
        let r: (u32, u32, u32) = row?;
        println!("{:?}", r);
    }

    Ok(())
}
