use nav_lib::Node;

type NodeConn = (u32, u32, u32);

/// map a room to the corresponding index
pub fn map_room(room: &Node, rooms_list: Vec<Node>) -> Option<u32> {
    rooms_list
        .iter()
        .position(|x| x.name == room.name)
        .map(|x| x as u32 + 1)
}

/// map index back to a room
pub fn map_index(index: u32, rooms_list: &[Node]) -> &Node {
    &rooms_list[index as usize - 1]
}

/// use dijkstra to find the path
pub fn find_path(connections: Vec<NodeConn>, start: u32, end: u32) -> (Vec<u32>, u32) {
    let find_neighbors = |current_node: &u32| {
        connections
            .iter()
            .filter(|possible_connection| {
                possible_connection.0 == *current_node || possible_connection.1 == *current_node
            })
            .into_iter()
            .map(|node| match node {
                (start, end, c) if start == current_node => (*end, *c),
                (start, _end, c) => (*start, *c),
            })
            .collect::<Vec<_>>()
    };

    dbg!(connections.clone());

    let path =
        pathfinding::directed::dijkstra::dijkstra(&start, find_neighbors, |node| *node == end);
    path.unwrap()
}

// Here, all current unit tests exist
#[cfg(test)]
mod tests {
    use crate::pathfind::find_path;

    #[test]
    fn test_same_id() {
        // This test checks whether the same room ID is not duplicated when the start and end room are the same.
        let conns = vec![(0, 0, 0)];
        let input2 = 0;
        let input3 = 0;
        let expected_output: (Vec<u32>, u32) = (vec![input3], 0); // Only one connection should be in the output vector.
        let output = find_path(conns, input2, input3);
        assert_eq!(output, expected_output);
    }

    #[test]
    fn test_same_path_different_distance() {
        // This test checks to see whether the shortest path is used when two paths have different distances.
        let conns = vec![(1, 2, 60), (1, 2, 4)];
        let input2 = 1;
        let input3 = 2;
        let expected_output: (Vec<u32>, u32) = (vec![input2, input3], 4); // As 4 < 60, the output distance from node 1 to 2 should be 4.
        let output = find_path(conns, input2, input3);
        assert_eq!(output, expected_output);
    }

    #[test]
    #[should_panic]
    fn test_no_connection() {
        // This test checks that an error is given if no path is found.
        let conns = vec![(0, 1, 1), (2, 3, 2)];
        let input2 = 0;
        let input3 = 3;
        find_path(conns, input2, input3);
    }
}

/* #[derive(Debug, Clone)]
struct DijkstraEntry {
    node: u32,         // node id
    dist: Option<u32>, // distance accumulated from the start
    prev: Option<u32>,
} */

/* tried by hand first, but edge cases were sufficiently supported
pub fn find_path(
    connections: Vec<NodeConn>,
    start: u32,
    end: u32,
    room_count: u32,
    rooms_list: Vec<Node>,
) -> Vec<NodeConn> {

    fn find_next<'a>(
        iter: impl Iterator<Item = &'a NodeConn>,
        start: u32,
    ) -> impl Iterator<Item = &'a NodeConn> {
        iter.filter(move |conn| conn.0 == start)
    }

    let connections: Vec<NodeConn> = {
        let backwards_nodes: Vec<(u32, u32, u32)> = connections
            .iter()
            .map(|node| (node.1, node.0, node.2))
            .collect();

        connections
            .iter()
            .chain(backwards_nodes.iter())
            .map(|x| *x)
            .collect::<Vec<_>>()
    };

    let mut dijkstra_table: Vec<DijkstraEntry> = (0..room_count)
        .map(|x| DijkstraEntry {
            node: x,
            dist: None,
            prev: None,
        })
        .collect();

    let mut Q_unvisited: Vec<u32> = (0..room_count).collect();

    dijkstra_table[start as usize].dist = Some(0);

    println!(
        "DT before everything: {:?}",
        dijkstra_table.iter().map(|x| x.node).collect::<Vec<_>>()
    );

    while !Q_unvisited.is_empty() {
        let u_closest = {
            let mut current = (-1, 0);

            for vertex in dijkstra_table.iter() {
                if vertex.dist.is_some()
                    && vertex.dist.unwrap() as i64 > current.0
                    && Q_unvisited.contains(&vertex.node)
                {
                    current.0 = vertex.dist.unwrap() as i64;
                    current.1 = vertex.node;
                }
            }

            // vertex in unvisited with the current minimum distance
            &dijkstra_table[current.1 as usize]
        };
        let u_closest = u_closest.clone();

        if u_closest.node == end {
            println!("Found end");
            break;
        }

        println!("found the next closest: {:?}", u_closest);
        Q_unvisited.remove(
            Q_unvisited
                .iter()
                .position(|x| *x == u_closest.node)
                .expect("Couldn't evaluate path"),
        );
        println!("unvisited nodes: {:?}", Q_unvisited);

        let neighbors: Vec<_> = find_next(connections.iter(), u_closest.node)
            .filter(|node| Q_unvisited.contains(&node.1))
            .collect();

        println!(
            "\nNEIGHBORS: {:#?}",
            neighbors
                .iter()
                .map(|x| format!("[{}]-{}->[{}]", x.0, x.2, x.1))
                .collect::<Vec<_>>()
        );

        for n in neighbors {
            let alt = u_closest.dist.unwrap() + n.2;
            let v = &mut dijkstra_table[n.1 as usize];
            if v.dist.is_none() || alt < v.dist.unwrap() {
                println!("Overwrote dist for id: {:?}", v.node);
                v.dist = Some(alt);
                v.prev = Some(u_closest.node);
            }
        }

        println!(
            "DT after something: {:#?}",
            dijkstra_table
                .iter()
                .map(|x| format!("{} <- {:?} | {:?}", x.node, x.prev, x.dist))
                .collect::<Vec<_>>()
        );
    }

    let mut path = vec![end];

    let mut next = end;
    while let DijkstraEntry {
        prev: Some(prev), ..
    } = dijkstra_table[next as usize]
    {
        path.push(prev);
        next = prev;
    }

    let pp = path.clone();

    let path = path
        .iter()
        .rev()
        .fold("|".into(), |acc, n| format!("{}->{}", acc, *n));

    println!("Path! {}", path);

    let path = pp.iter().rev().fold("|".into(), |acc, idx| {
        format!("{}->{}", acc, map_index(*idx, &rooms_list).name)
    });

    println!("PATH: {}", path);

    println!(
        "with Length: {}",
        dijkstra_table[end as usize].dist.unwrap()
    );
    /*
    let next_nodes = find_next(connections.iter(), start);

    for node in next_nodes {
        let entry = &mut dijkstra_table[node.1 as usize - 1];

        if entry.dist.is_none() || entry.dist.unwrap() < node.2 {
            entry.dist = Some()
        }


    } */

    vec![]
}
*/
