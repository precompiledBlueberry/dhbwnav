use axum::{
    extract::Path,
    http::StatusCode,
    routing::{get, get_service, post, MethodRouter},
    Router,
};
use nav_lib::{Node, Query};
use rusqlite::{Connection as DbConnection, OpenFlags, Result};
use tokio::io::AsyncReadExt;
use tower_http::services::ServeFile;

mod pathfind;

#[tokio::main]
async fn main() {
    // serve this specific file with additional error handling
    let serve_file = |path: &str| -> MethodRouter {
        get_service(ServeFile::new(path)).handle_error(|error: std::io::Error| async move {
            eprintln!("ERR {}", error);
            (
                StatusCode::INTERNAL_SERVER_ERROR,
                format!("Unhandled internal error: {}", error),
            )
        })
    };

    // our router
    let app = Router::new()
        .route("/", serve_file("nav_app/index.html"))
        .route("/light-mode.css", serve_file("nav_app/light-mode.css"))
        .route(
            "/pkg/nav_app_bg.wasm",
            serve_file("nav_app/pkg/nav_app_bg.wasm"),
        )
        .route("/pkg/nav_app.js", serve_file("nav_app/pkg/nav_app.js"))
        .route("/assets/:name", get(give_file))
        .route("/user_submit", post(user_submit))
        .route("/fetch_list", get(fetch_list));

    // run it with hyper on localhost:3000
    axum::Server::bind(&"0.0.0.0:8000".parse().unwrap())
        .serve(app.into_make_service())
        .await
        .unwrap();
}

/// serve a file found in assets directory
async fn give_file(Path(path): Path<String>) -> Vec<u8> {
    let mut data = Vec::new();
    tokio::fs::File::open(std::path::Path::new("./nav_app/assets").join(path))
        .await
        .unwrap()
        .read_to_end(&mut data) //read binary data; not strings
        .await
        .unwrap();
    data
}

/// handle the submitted user data and respond with a found path
async fn user_submit(query: String) -> String {
    let Query { start, end } = serde_json::from_str(&query).expect("Deser failed");
    println!("{:?} -> {:?}?", start.name, end.name);

    // get all rooms
    let rooms_list = {
        let db = DbConnection::open_with_flags(
            "nav_server/database/navDB.db",
            OpenFlags::SQLITE_OPEN_READ_WRITE,
        )
        .unwrap();
        query_rooms(&db).unwrap()
    };

    // get all connections
    let node_list: Vec<_> = {
        let db = DbConnection::open_with_flags(
            "nav_server/database/navDB.db",
            OpenFlags::SQLITE_OPEN_READ_WRITE,
        )
        .unwrap();
        query_conns(&db).unwrap()
    };

    // TODO: REMOVE .clone cuz redundanté
    let start_id = pathfind::map_room(&start, rooms_list.clone()).unwrap();
    let end_id = pathfind::map_room(&end, rooms_list.clone()).unwrap();

    // find a path from the start node to the end node and output it
    let graph = pathfind::find_path(node_list, start_id, end_id);
    let path = graph.0.iter().fold("".into(), |acc, n| {
        format!("{acc},{}", pathfind::map_index(*n, &rooms_list).name)
    });
    format!("{}:{}", graph.1, path)
}

/// fetch the room data and respond to the client with that data
async fn fetch_list() -> String {
    let db = DbConnection::open_with_flags(
        "nav_server/database/navDB.db",
        OpenFlags::SQLITE_OPEN_READ_WRITE,
    )
    .unwrap();

    let node_list = query_rooms(&db).unwrap();
    println!("Fetched this list from DB: {:?}", node_list);

    db.close().unwrap();

    serde_json::to_string(&node_list).expect("Couldn't jsonify the connections :(")
}

/// query all connections form the provided database
fn query_conns(db: &DbConnection) -> Result<Vec<(u32, u32, u32)>> {
    let mut stmt = db.prepare("SELECT * FROM conns")?;

    let rows = stmt.query_map([], |row| Ok((row.get(1)?, row.get(2)?, row.get(3)?)))?;

    let mut conn_data = Vec::new();
    for row in rows {
        conn_data.push(row?);
    }

    Ok(conn_data)
}

/// query all room data form the provided database
fn query_rooms(db: &DbConnection) -> Result<Vec<Node>> {
    let mut stmt = db.prepare("SELECT * FROM rooms")?;

    let rows: Vec<_> = stmt
        .query_map([], |row| -> Result<Node> {
            let name = row.get(1)?;
            let floor = row.get(2)?;
            let building = row.get(3)?;
            let node_type = row.get(4)?;

            Ok(Node {
                name,
                floor,
                building,
                node_type,
            })
        })?
        .map(|r| r.unwrap())
        .collect();

    Ok(rows)
}
