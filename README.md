# DHBWNav
The DHBW Navigation web app is an all-device tool to help newcomers and regular students alike to find their way around the DHBW Mannheim.

# Software Requirements

+ [Rust and cargo](https://www.rust-lang.org/learn/get-started) - the rust programming language and its toolchain
+ [wasm-pack](https://rustwasm.github.io/wasm-pack/installer/) - compilation and glue-code-generation for Web-Assembly
+ [basic-http-server](https://github.com/brson/basic-http-server) - launching simple html files in localhost (only necessary for debugging purposes)

# Architecture

- nav_app: Contains the web app frontend.
- nav_lib: The API to integrate the backend functionalities into the website.
- nav_server: Server code 

# Debug compilation and execution

To compile the server and API, simply run the command `cargo r` or `cargo run`.
This builds the library that the webserver uses, and runs the server.

To compile the web app, Web-Assembly must be used. To achieve this, use the command `wasm-pack build -t web nav_app` in the project root. To launch the web app without nav_server for debugging purposes, use the command `basic-http-server ./nav_app/` instead.

# Running Unit-Tests

Unit tests, as of now, are integrated in nav_lib. In order to execute the tests run `cargo test` in either the project root or in nav_lib directory.
[cargo-clippy](https://github.com/rust-lang/rust-clippy) is also used in order to catch common bugs.

# In-Depth Documentation

To See more in-depth documentation, look up the [docs](./docs/) folder.
