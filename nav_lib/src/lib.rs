use std::{error::Error, fmt::Display, str::FromStr};

use serde::{Deserialize, Serialize};

mod sql_interop;

/// Stores information about a room
#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct Node {
    pub floor: u32, // could be u8 but this decreases complexity with SQL-interop
    pub name: String,
    pub building: Building,
    pub node_type: NodeType,
}

/// represents the query, that is created by a user
#[derive(Serialize, Deserialize, Debug)]
pub struct Query {
    /// where to start off
    pub start: Node,
    /// destination, the user wants to arrive at
    pub end: Node,
}

impl Query {
    pub fn new(start: &Node, end: &Node) -> Self {
        Self {
            start: start.to_owned(),
            end: end.to_owned(),
        }
    }
}

/// enumeration of all possible building types in the uni
#[derive(Clone, Serialize, Deserialize, Debug)]
pub enum Building {
    A,
    B,
    C,
    D,
    E,
}

/// The uni consists of several types of rooms.
/// This is represented here.
#[derive(Clone, Serialize, Deserialize, Debug)]
pub enum NodeType {
    Cantine,
    Office, // secretary, lecturer's office, ..
    LectureRoom,
    Ingress, // point where you arrive
    Maintenance,
    Stairwell,
    Toilet,
}

/// Create a self-implemented error-type so that it can be raised and
/// shows more information in case of a panic
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ParseEnumError;
impl Display for ParseEnumError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        "Couldn't parse the enum".fmt(f)
    }
}
impl Error for ParseEnumError {}

impl ToString for Building {
    fn to_string(&self) -> String {
        use Building::*;
        match self {
            A => "A".to_string(),
            B => "B".to_string(),
            C => "C".to_string(),
            D => "D".to_string(),
            E => "E".to_string(),
        }
    }
}

impl ToString for NodeType {
    fn to_string(&self) -> String {
        use NodeType::*;
        match self {
            Cantine => "Cantine".to_string(),
            Office => "Office".to_string(),
            LectureRoom => "LectureRoom".to_string(),
            Ingress => "Ingress".to_string(),
            Maintenance => "Maintenance".to_string(),
            Stairwell => "Stairwell".to_string(),
            Toilet => "Toilet".to_string(),
        }
    }
}

impl FromStr for Building {
    type Err = ParseEnumError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "A" => Ok(Building::A),
            "B" => Ok(Building::B),
            "C" => Ok(Building::C),
            "D" => Ok(Building::D),
            "E" => Ok(Building::E),
            _ => Err(ParseEnumError),
        }
    }
}

impl FromStr for NodeType {
    type Err = ParseEnumError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "Cantine" => Ok(NodeType::Cantine),
            "Office" => Ok(NodeType::Office),
            "LectureRoom" => Ok(NodeType::LectureRoom),
            "Ingress" => Ok(NodeType::Ingress),
            "Maintenance" => Ok(NodeType::Maintenance),
            "Stairwell" => Ok(NodeType::Stairwell),
            "Toilet" => Ok(NodeType::Toilet),
            _ => Err(ParseEnumError),
        }
    }
}
